CPPFLAGS=-O3
CC=g++
EXECNAME=planner
INCLUDE=-Igetoptcpp
LFLAGS=-lgetoptcpp 

all: planner

planner: planner.o libgetoptcpp.so
	$(CC) $(CFLAGS) $(INCLUDE)  $< -o $(EXECNAME) $(LFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@ $(LFLAGS)

libgetoptcpp.so:
	cd getoptcpp/ && $(MAKE)

clean:
	$(RM) planner.o