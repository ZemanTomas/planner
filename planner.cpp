#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "getoptcpp.hpp"

using namespace std;

struct Block{
    Block(uint64_t data){
        Block tmp = *(Block*)&data;
        size = tmp.size;
        year = tmp.year;
        month = tmp.month;
        day = tmp.day;
        hour = tmp.hour;
        minute = tmp.minute;
    }
    uint16_t size;
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;

} __attribute__((packed));

map<uint64_t,string> planner;
fstream planfile;

void displayBlock(uint64_t id, Block b, string text = "");
void store(const string filename);
void load(const string filename);

int main(int argc, char** argv){
    argParser parser = argParser(argc,argv);
    parser.addOptionLong("verbose",false,'v');
    parser.addOptionLong("list",false,'l');
    parser.addOptionLong("new",true,0);
    parser.addOptionLong("remove",true,0);
    parser.addOptionLong("file",true,0);
    
    parser.eval();


    string file = "plan.bin";
    if(parser.isSet("file")){
        file = parser.get("file");
    }


    load(file);

    if(parser.isSet("list")){
        for(auto& it : planner){displayBlock(it.first,it.first,it.second);}
    }
    


    store(file);
}

void displayBlock(uint64_t id, Block b, string text){
    printf("%lu: %u02.%u02.%u04 %u02:%u02 (%u) %s\n",id,b.day,b.month,b.year,b.hour,b.minute,b.size,text.c_str());
}

void store(const string filename){
    planfile.open(filename,ios::trunc | std::fstream::out);
    for(auto& it : planner){
        planfile << it.first << "\n" << it.second << "\n";
    }

    planfile.close();
}

void load(const string filename){
    planfile.open(filename);
    uint64_t num;
    string tmp,text;
    while(getline(planfile,tmp)){
        num = stoull(tmp);
        getline(planfile,text);
        planner[num] = text;
    }
    planfile.close();
}